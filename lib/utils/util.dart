import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../model/sys_config.dart';

void alert(
    {required BuildContext context,
    String title: 'Perhatian',
    required List<Widget> children}) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          titlePadding: const EdgeInsets.fromLTRB(12, 24.0, 24.0, 0.0),
          contentPadding: const EdgeInsets.all(12),
          title: Text(title),
          children: children,
        );
      });
}

void alertAct(
    {required BuildContext context,
    bool barrierDismissible: true,
    String title: 'Perhatian',
    required Widget content,
    required List<Widget> actions}) {
  showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          titlePadding: EdgeInsets.fromLTRB(12, 24.0, 24.0, 0.0),
          contentPadding: EdgeInsets.all(12),
          title: Text(title),
          content: content,
          actions: actions,
        );
      });
}

String dayNameIndo(String hari) {
  switch (hari) {
    case 'Monday':
      hari = 'Senin';
      break;

    case 'Tuesday':
      hari = 'Selasa';
      break;

    case 'Wednesday':
      hari = 'Rabu';
      break;

    case 'Thursday':
      hari = 'Kamis';
      break;

    case 'Friday':
      hari = 'Jum\'at';
      break;

    case 'Saturday':
      hari = 'Sabtu';
      break;

    case 'Sunday':
      hari = 'Minggu';
      break;
  }
  return hari;
}

String monthNameIndo(bulan) {
  switch (bulan) {
    case 'January':
      bulan = 'Januari';
      break;

    case 'February':
      bulan = 'Februari';
      break;

    case 'March':
      bulan = 'Maret';
      break;

    case 'May':
      bulan = 'Mei';
      break;

    case 'June':
      bulan = 'Juni';
      break;

    case 'July':
      bulan = 'Juli';
      break;

    case 'August':
      bulan = 'Agustus';
      break;

    case 'September':
      bulan = 'September';
      break;

    case 'October':
      bulan = 'Oktober';
      break;

    case 'December':
      bulan = 'Desember';
      break;
  }
  return bulan;
}

String dateIndo(String date) {
  List<String> dateArr = date.split(', ');
  String hari = dateArr[0], bulanTanggal = dateArr[1], tahun = dateArr[2];
  List<String> bulanTanggalArr = bulanTanggal.split(' ');
  String bulan = bulanTanggalArr[0], tanggal = bulanTanggalArr[1];
  hari = dayNameIndo(hari);
  bulan = (bulan);

  return "$hari, $tanggal $bulan $tahun";
}

String dateOnlyIndo(String date) {
  List<String> dateArr = date.split(', ');
  String hari = dateArr[0], bulanTanggal = dateArr[1], tahun = dateArr[2];
  List<String> bulanTanggalArr = bulanTanggal.split(' ');
  String bulan = bulanTanggalArr[0], tanggal = bulanTanggalArr[1];
  hari = dayNameIndo(hari);
  bulan = monthNameIndo(bulan);

  return "$tanggal $bulan $tahun";
}

extension DateTimeExt on DateTime {
  bool isWorkingDay() {
    bool isSeninSdJumat = (weekday >= 1 && weekday <= 5);
    bool isTanggalMerah = false;
    SysConfig.listTanggalMerah().forEach((tgl) {
      if (this.isSameDate(tgl)) isTanggalMerah = true;
    });
    // print('this : ${DateFormat('yyyy-MM-dd').format(this)}');
    // print('isSeninSdJumat : $isSeninSdJumat');
    // print('isTanggalMerah : $isTanggalMerah');
    return (isSeninSdJumat && !isTanggalMerah);
  }

  bool isSameDate(DateTime other) {
    return this.year == other.year &&
        this.month == other.month &&
        this.day == other.day;
  }
}

enum RadioDay { work_day, all_day, overtime_only }

String formatDate(DateTime date) {
  return "${date.year}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}";
}

List<DateTime> getDaysInBeteween(DateTime? startDate, DateTime? endDate,
    {RadioDay selectedRadioDay: RadioDay.work_day}) {
  List<DateTime> days = [];
  if (startDate != null && endDate != null) {
    // print(
    //     'cari selisih hari dari ${DateFormat.yMMMMEEEEd().format(startDate)} hingga ${DateFormat.yMMMMEEEEd().format(endDate)}');
    for (int i = 0; i <= endDate.difference(startDate).inDays; i++) {
      if (selectedRadioDay == RadioDay.work_day) {
        // jik hari kerja (bukan tgl merah), masukkan ke list
        if (startDate.add(Duration(days: i)).isWorkingDay()) {
          days.add(startDate.add(Duration(days: i)));
        }
        // jika all day
      } else {
        days.add(startDate.add(Duration(days: i)));
      }
    }
  }
  return days;
}
