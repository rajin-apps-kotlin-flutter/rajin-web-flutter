import 'package:flutter/material.dart';
import '../../../model/staff.dart';
import '../../model/unit_kerja.dart';
import 'organisasi_form.dart';

class OrganisasiTable extends StatefulWidget {
  List<UnitKerja> unitKerjas;
  Staff staffSession;
  VoidCallback reloadData;

  OrganisasiTable(
      {required this.unitKerjas,
      required this.staffSession,
      required this.reloadData,
      Key? key})
      : super(key: key);

  @override
  OrganisasiTableState createState() => OrganisasiTableState();
}

class OrganisasiTableState extends State<OrganisasiTable> {
  List<DateTime>? listDateTime;

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    int indexTable = 0;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: screenSize.width / 15),
      child: Card(
        child: Align(
          alignment: Alignment.centerLeft,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: DataTable(
              columns: <DataColumn>[
                _renderCol('#'),
                _renderCol('INSTANSI'),
                _renderCol('AKUN ADMINISTRATOR'),
                _renderCol('AKSI'),
              ],
              rows: widget.unitKerjas.map((UnitKerja unitKerja) {
                indexTable++;

                return DataRow(cells: [
                  DataCell(Text('$indexTable')),
                  DataCell(Text('${unitKerja.nama}')),
                  DataCell(Text(unitKerja.adminStaffToString())),
                  DataCell(Row(children: [
                    TextButton.icon(
                      icon: const Icon(Icons.edit, color: Colors.blue),
                      label: const Text('EDIT',
                          style: TextStyle(color: Colors.blue)),
                      onPressed: () async {
                        await showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (context) {
                              return OrganisasiForm(
                                  unitKerja: unitKerja,
                                  staffSession: widget.staffSession);
                            });
                        widget.reloadData();
                      },
                    ),
                  ]))
                ]);
              }).toList(),
            ),
          ),
        ),
      ),
    );
  }

  DataColumn _renderCol(String label) {
    return DataColumn(
      label: Text(
        label,
        style: const TextStyle(fontStyle: FontStyle.italic),
      ),
    );
  }
}
