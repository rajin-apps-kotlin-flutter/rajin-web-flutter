import 'package:flutter/material.dart';

import '../../model/staff.dart';
import '../../model/unit_kerja.dart';
import 'staff_form.dart';

class StaffTable extends StatefulWidget {
  final UnitKerja selectedUnitKerja;
  List<Staff> staffs;
  Staff staffSession;
  VoidCallback reloadData;

  StaffTable(
      {required this.staffs,
      required this.selectedUnitKerja,
      required this.staffSession,
      required this.reloadData,
      Key? key})
      : super(key: key);

  @override
  StaffTableState createState() => StaffTableState();
}

class StaffTableState extends State<StaffTable> {
  List<DateTime>? listDateTime;

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    int indexTable = 0;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: screenSize.width / 15),
      child: Card(
        child: Align(
          alignment: Alignment.centerLeft,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: DataTable(
              columns: <DataColumn>[
                _renderCol('#'),
                _renderCol('NOMOR INDUK'),
                _renderCol('NAMA'),
                _renderCol('WAKTU REGISTRASI'),
                _renderCol('UNIT KERJA'),
                _renderCol('AKSI'),
              ],
              rows: widget.staffs.map((Staff staff) {
                indexTable++;

                return DataRow(cells: [
                  DataCell(Text('$indexTable')),
                  DataCell(SelectableText('${staff.noInduk}')),
                  DataCell(Text('${staff.nama}')),
                  DataCell(Text('${staff.timeCreate?.toDate()}')),
                  DataCell(Text('${widget.selectedUnitKerja.nama}')),
                  DataCell(Row(children: [
                    TextButton.icon(
                      icon: const Icon(Icons.edit, color: Colors.blue),
                      label: const Text('EDIT',
                          style: TextStyle(color: Colors.blue)),
                      onPressed: () async {
                        await showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (context) {
                              return StaffForm(
                                  staff: staff,
                                  staffSession: widget.staffSession);
                            });
                        widget.reloadData();
                      },
                    ),
                  ]))
                ]);
              }).toList(),
            ),
          ),
        ),
      ),
    );
  }

  DataColumn _renderCol(String label) {
    return DataColumn(
      label: Text(
        label,
        style: const TextStyle(fontStyle: FontStyle.italic),
      ),
    );
  }
}
