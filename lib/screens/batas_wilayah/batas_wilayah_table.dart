import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import '../../../utils/util.dart';
import '../../../model/staff.dart';
import '../../model/batas_wilayah.dart';
import '../../model/unit_kerja.dart';
import 'form/batas_wilayah_set_polygon.dart';

class BatasWilayahTable extends StatefulWidget {
  List<BatasWilayah> batasWilayah;
  Staff staffSession;
  VoidCallback reloadData;

  BatasWilayahTable(
      {required this.batasWilayah,
      required this.staffSession,
      required this.reloadData,
      Key? key})
      : super(key: key);

  @override
  BatasWilayahTableState createState() => BatasWilayahTableState();
}

class BatasWilayahTableState extends State<BatasWilayahTable> {
  List<DateTime>? listDateTime;

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    int _indexTable = 0;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: screenSize.width / 15),
      child: Card(
        child: Align(
          alignment: Alignment.centerLeft,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: DataTable(
              columns: <DataColumn>[
                _renderCol('#'),
                _renderCol('NAMA'),
                _renderCol('AKSI'),
              ],
              rows: widget.batasWilayah.map((BatasWilayah batasWilayah) {
                _indexTable++;

                return DataRow(cells: [
                  DataCell(Text('$_indexTable')),
                  DataCell(Text('${batasWilayah.nama}')),
                  DataCell(Row(children: [
                    TextButton.icon(
                      icon: const Icon(Icons.edit, color: Colors.blue),
                      label: const Text('EDIT POLIGON',
                          style: TextStyle(color: Colors.blue)),
                      onPressed: () async {
                        bool res =
                            await Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => BatasWilayahSetPolygon(
                                      wilayah: batasWilayah,
                                      staffSession: widget.staffSession,
                                    )));
                        widget.reloadData();
                      },
                    ),
                    const SizedBox(width: 20),
                    TextButton.icon(
                      icon: const Icon(Icons.edit, color: Colors.blue),
                      label: const Text('HAPUS',
                          style: TextStyle(color: Colors.blue)),
                      onPressed: () => alertAct(
                          context: context,
                          content: const Text('Anda yakin ingin menghapus?'),
                          actions: [
                            TextButton(
                                onPressed: () {
                                  deleteRecord(batasWilayah.nama!)
                                      .then((value) {
                                    Navigator.of(context).pop(true);
                                    widget.reloadData();
                                  });
                                },
                                child: const Text('YA')),
                            TextButton(
                                onPressed: () => Navigator.of(context).pop(),
                                child: const Text('BATAL'))
                          ]),
                    ),
                  ]))
                ]);
              }).toList(),
            ),
          ),
        ),
      ),
    );
  }

  DataColumn _renderCol(String label) {
    return DataColumn(
      label: Text(
        '$label',
        style: TextStyle(fontStyle: FontStyle.italic),
      ),
    );
  }

  Future deleteRecord(String nama) async {
    try {
      DocumentReference ref = FirebaseFirestore.instance
          .collection(UnitKerja.collectionName)
          .doc(widget.staffSession.unitKerjaParent!.id);
      DocumentSnapshot snapshot = await ref.get();

      UnitKerja unitKerja = UnitKerja.fromJson(
          snapshot.data() as Map<String, dynamic>, snapshot.id);

      unitKerja.batasWilayah?.removeWhere((b) => b.nama == nama);

      ref.update({
        'batas_wilayah':
            unitKerja.batasWilayah?.map((b) => b.toJson()).toList() ?? [],
        'time_update': FieldValue.serverTimestamp(),
      });
      return true;
    } catch (e) {
      print(e.toString());
    }
    return false;
  }
}
