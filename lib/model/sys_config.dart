import 'jam_kerja.dart';
import 'presensi.dart';
import '../utils/util.dart';

Map<String, dynamic> signInSuperUser(String email, String password) {
  if (email == 'root' && password == 'root') {
    Map<String, dynamic> superUser = {
      'UID': null,
      'is_aktif': true,
      'is_super_user': true,
      'nama': 'Super User',
      'no_induk': '01',
      'unit_kerja_parent_name': 'RAJIN APPS SU'
    };
    return superUser;
  }
  return {};
}

MasterJamKerja getMasterJamkerja(
    DateTime dateTime, Presensi? presensi, List<JamKerja> jamKerjas) {
  List<MasterJamKerja> listJamKerja = jamKerjas.map((j) {
    var jamMasuk = j.masuk.split(':');
    var jamPulang = j.pulang.split(':');
    return MasterJamKerja(
        weekday: j.weekday,
        jamMasuk: DateTime(
            dateTime.year,
            dateTime.month,
            dateTime.day,
            int.parse(jamMasuk[0]),
            int.parse(jamMasuk[1]),
            int.parse(jamMasuk[2])),
        jamPulang: DateTime(
            dateTime.year,
            dateTime.month,
            dateTime.day,
            int.parse(jamPulang[0]),
            int.parse(jamPulang[1]),
            int.parse(jamPulang[2])));
  }).toList();

  return listJamKerja.firstWhere(
      (jamKerja) =>
          jamKerja.weekday == dateTime.weekday && dateTime.isWorkingDay(),
      orElse: () => MasterJamKerja(
          weekday: dateTime.weekday,
          jamMasuk:
              DateTime(dateTime.year, dateTime.month, dateTime.day, 07, 59, 00),
          jamPulang: DateTime(
              dateTime.year, dateTime.month, dateTime.day, 16, 00, 00)));
}

class SysConfig {
  //setup tanggal merah
  static List<dynamic> listTanggalMerah() {
    final List<dynamic> list = [
      DateTime(2023, 08, 17),
      DateTime(2023, 09, 28),
      DateTime(2023, 12, 25),
      DateTime(2023, 12, 26),
      DateTime(2024, 01, 01),
      DateTime(2024, 02, 08),
      // TODO: tambahkan tanggal libur nasional di sini
    ];

    return list;
  }
}

class MasterJamKerja {
  final int weekday;
  DateTime jamMasuk, jamPulang;

  MasterJamKerja(
      {required this.weekday, required this.jamMasuk, required this.jamPulang});
}
